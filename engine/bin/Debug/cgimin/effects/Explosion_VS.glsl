#version 330
precision highp float;

// input aus der VAO-Datenstruktur
in vec3 in_position;
in vec2 in_uv;
in vec2 in_size;
in vec3 in_direction;
in float in_life;

// Parameter
uniform mat4 projection_matrix;
uniform mat4 model_view_matrix;
uniform float update;

// "texcoord" wird an den Fragment-Shader weitergegeben, daher als "out" deklariert
out vec2 texcoord;

void main()
{
	int index = int((1.0 - (in_life - update) / in_life) * 48);
	if (index > 60) index = 60;
	
	vec2 uv = vec2((index % 8 + in_uv.x) / 8.0f, (index / 8 + in_uv.y) / 8.0f);

	texcoord = uv;

	vec4 pos = model_view_matrix * vec4(in_position + in_direction * update, 1);
	pos.x += in_size.x;
	pos.y += in_size.y;

	gl_Position = projection_matrix * pos;
}


