#version 330
precision highp float;

uniform sampler2D normal_texture;
uniform samplerCube cube_texture;

uniform mat4 model_view_matrix;

uniform vec3 light_direction;
uniform vec4 ambient_color;
uniform vec4 diffuse_color;

uniform vec4 camera_position;

in vec2 texcoord;
in vec2 texcoord2;

in mat3 fragTBN;
in vec3 fragV;
in vec4 viewPosition;

uniform float fogStart;
uniform float fogEnd;
uniform vec3 fogColor;

out vec4 outputColor;

void main()
{
    vec3 normal1 = texture(normal_texture, texcoord).rgb;
	normal1 = normalize(normal1 * 2.0 - 1.0); 
	normal1 = normalize(fragTBN * normal1); 

	vec3 normal2 = texture(normal_texture, texcoord2).rgb;
	normal2 = normalize(normal2 * 2.0 - 1.0); 
	normal2 = normalize(fragTBN * normal2); 

	vec3 normal = normalize(normal1 + normal2);

	float fakeFresnel = 1.0 - clamp((vec4(normal, 1) * model_view_matrix).z, 0, 1); 
	fakeFresnel = pow(fakeFresnel, 2.0f);

	vec3 texc = normalize(reflect(fragV, normal));
	vec4 cubeColor = texture(cube_texture, texc);

	float brightness = clamp(dot(normalize(normal), light_direction), 0, 1);

    outputColor = ambient_color +  brightness * diffuse_color + cubeColor * fakeFresnel;
	
	float fogFactor = (fogEnd - length(viewPosition.xyz)) / (fogEnd - fogStart);
	fogFactor = clamp(fogFactor, 0, 1);
	outputColor = fogFactor * outputColor + ((1 - fogFactor) * vec4(fogColor, 1));
}