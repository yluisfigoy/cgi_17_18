#version 330
precision highp float;

// input aus der VAO-Datenstruktur
in vec3 in_position;

// "modelview_projection_matrix" wird als Parameter erwartet, vom Typ Matrix4
uniform mat4 view_projection_matrix;
uniform mat4 model_matrix;
uniform mat4 model_view_matrix;
uniform vec4 camera_position;
uniform float update;

uniform float wavelength_x;
uniform float wavelength_z;
uniform float waveheight;

uniform float texture_scale;
uniform float texture_speed;

// "texcoord" wird an den Fragment-Shader weitergegeben, daher als "out" deklariert
out vec2 texcoord;
out vec2 texcoord2;

out vec3 fragV;
out vec4 viewPosition;
out mat3 fragTBN;

void main()
{
	// "in_uv" (Texturkoordinate) wird direkt an den Fragment-Shader weitergereicht

	vec4 globalPos = model_matrix * vec4(in_position, 1);	
	
	float baseX = sin(update + globalPos.x * wavelength_x) * waveheight;
	float baseY = cos(update + globalPos.z * wavelength_z) * waveheight;

	globalPos.y = globalPos.y + baseX + baseY;

	vec3 T = normalize(vec3(1.0f, (sin(update + globalPos.x * wavelength_x - 0.5f) * waveheight + baseY) - (sin(update + globalPos.x * wavelength_x + 0.5f) * waveheight + baseY), 0)); 
	vec3 B = normalize(vec3(0, (baseX + cos(update + globalPos.z * wavelength_z - 0.5f) * waveheight) - (baseX + cos(update + globalPos.z * wavelength_z + 0.5f) * waveheight), 1.0f));
    vec3 N = normalize(cross(B, T));
    fragTBN = mat3(T, B, N);

	fragV = vec3(-normalize(camera_position - globalPos));
	fragV.x = -fragV.x;

	float txSpeed1 = update * texture_speed;
	float txSpeed2 = update * texture_speed * 1.1f;
	texcoord = globalPos.xz * texture_scale + vec2(txSpeed1, txSpeed1);
	texcoord2 = globalPos.xz * texture_scale * 1.11f + vec2(txSpeed2, txSpeed2);

	// in gl_Position die finalan Vertex-Position geschrieben ("modelview_projection_matrix" * "in_position")
	viewPosition = model_view_matrix * vec4(in_position,1);
	gl_Position = view_projection_matrix * globalPos;
}


