#version 330
precision highp float;

// input aus der VAO-Datenstruktur
in vec3 in_position;
in vec3 in_normal; 
in vec2 in_uv;

// "modelview_projection_matrix" wird als Parameter erwartet, vom Typ Matrix4
uniform mat4 modelview_projection_matrix;

// Die "Model-Matrix" wird als Parameter erwaretet, um die TBN-Matrix in den World-Space zu transformieren
uniform mat4 model_matrix;

uniform mat4 model_view_matrix;

// Kamera-Position wird �bergeben
uniform vec4 camera_position;

// Lichtrichtung
uniform vec3 light_direction;

uniform float fresnel_value;

out vec2 fragTexcoord;
out vec3 fragCubeTexCoord;

out vec4 viewPosition;
out float brightness;

out float fresnel;

void main()
{
	
	// "in_uv" (Texturkoordinate) wird direkt an den Fragment-Shader weitergereicht.
	fragTexcoord = in_uv;

	vec3 normal = vec3(model_matrix * vec4(in_normal, 0.0));

	// position �bergeben
	vec3 fragV = vec3(-normalize(camera_position - model_matrix *  vec4(in_position,1)));
	fragV.x = -fragV.x;
	fragCubeTexCoord = normalize(reflect(fragV, normal));

	vec3 h = normalize(light_direction - fragV);
	fresnel = 1.0 - dot(-fragV, h);
	fresnel = pow(fresnel, 5.0);
	fresnel += fresnel_value * (1.0 - fresnel);

	brightness = clamp(dot(normalize(normal), light_direction), 0, 1);

	// in gl_Position die finalan Vertex-Position geschrieben ("modelview_projection_matrix" * "in_position")
	gl_Position = modelview_projection_matrix * vec4(in_position, 1);
	viewPosition = model_view_matrix * vec4(in_position, 1.0);
}


