#version 330
precision highp float;

// input from VAO data structure
in vec3 in_position;
in vec3 in_normal; 
in vec2 in_uv; 

// "modelview_projection_matrix" parameter
uniform mat4 modelview_projection_matrix;

// camera-position
uniform vec4 camera_position;

// "model_matrix""
uniform mat4 model_matrix;

// "texcoord" is given to Fragment-Shader
out vec2 fragTexcoord;

// the normal is also given to the Fragment-Shader
out vec3 fragNormal;
out vec3 fragV;

void main()
{
	fragTexcoord = in_uv;

	fragNormal = vec3(model_matrix * vec4(in_normal, 0.0));;

    vec4 vertPosition4 = model_matrix * vec4(in_position, 1.0);
	fragV = vec3(vertPosition4) / vertPosition4.w;

	gl_Position = modelview_projection_matrix * vec4(in_position, 1);
}


