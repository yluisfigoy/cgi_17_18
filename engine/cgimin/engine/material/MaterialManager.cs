﻿using System;
using System.Threading;
using cgimin.engine.material.ambientdiffuse;
using cgimin.engine.material.cubereflectionnormal;
using cgimin.engine.material.normalmapping;
using cgimin.engine.material.normalmappingcubespecular;
using cgimin.engine.material.simplereflection;
using cgimin.engine.material.simpletexture;
using Engine.cgimin.engine.material.simpleblend;

namespace cgimin.engine.material
{
	/// <summary>
	/// An Enum of all available Materials in the order of which they are drawn.
	/// </summary>
	public enum Material
	{
		//ORDER MATTERS!
		NORMAL_MAPPING_CUBE_SPECULAR = 0,
		CUBE_REFLECTION_NORMAL = 1,
		NORMAL_MAPPING = 2,
		SIMPLE_REFLECTION = 3,
		SIMPLE_TEXTURE = 4,
		AMBIENT_DIFFUSE = 5,
		AMBIENT_DIFFUSE_SPECULAR = 6,
		SIMPLE_BLEND = 7
		//ORDER MATTERS!
	}

	public static class MaterialManager
	{
		private static readonly BaseMaterial[] Materials;

		static MaterialManager()
		{
			Materials = new BaseMaterial[Enum.GetNames(typeof(Material)).Length];

			Materials[(int) Material.NORMAL_MAPPING_CUBE_SPECULAR] = new NormalMappingCubeSpecularMaterial();
			Materials[(int) Material.CUBE_REFLECTION_NORMAL] = new CubeReflectionNormalMaterial();
			Materials[(int) Material.NORMAL_MAPPING] = new NormalMappingMaterial();
			Materials[(int) Material.SIMPLE_REFLECTION] = new SimpleReflectionMaterial();
			Materials[(int) Material.SIMPLE_TEXTURE] = new SimpleTextureMaterial();
			Materials[(int) Material.AMBIENT_DIFFUSE] = new AmbientDiffuseMaterial();
			Materials[(int) Material.AMBIENT_DIFFUSE_SPECULAR] = new AmbientDiffuseSpecularMaterial();
			Materials[(int) Material.SIMPLE_BLEND] = new SimpleBlendMaterial();
		}

		public static BaseMaterial GetMaterial(Material material) => Materials[(int) material];

		/// <summary>
		/// Draws all Materials in succession.
		/// </summary>
		public static void DrawAll()
		{
			foreach (var material in Materials)
			{
				material.DrawAll();
			}
		}
	}
}