#version 330
precision highp float;

uniform sampler2D sampler; 

// "model_matrix" Matrix as parameter
uniform mat4 model_matrix;

// Parameter for direktional light
uniform vec3 light_direction;
uniform vec4 light_ambient_color;
uniform vec4 light_diffuse_color;
uniform vec4 light_specular_color;

// Parameter light for "Shininess"
uniform float specular_shininess;

// input from Vertex-Shader
in vec2 fragTexcoord;
in vec3 fragNormal;
in vec3 fragV;

// final color
out vec4 outputColor;

void main()
{	
	// calculate vertex normal
    vec3 normal = normalize(fragNormal);

	// calculate the half-vector
	float lambertian = max(dot(light_direction, normal), 0.0);
    float specular = 0.0;
        
    if (lambertian > 0.0) {
        vec3 viewDir = normalize(-fragV);
        vec3 halfDir = normalize(light_direction + viewDir);
        float specAngle = max(dot(halfDir, normal), 0.0);
        specular = pow(specAngle, specular_shininess);
    }
    
    vec4 colorLinear = light_ambient_color +
                       lambertian * light_diffuse_color +
                       specular * light_specular_color;
                       
    vec4 surfaceColor = texture2D(sampler, fragTexcoord);
    
    outputColor = colorLinear * surfaceColor;
}